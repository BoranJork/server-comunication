
import {Container,Form,Row,Col,Button,Table} from 'react-bootstrap'
import React,{useState,useEffect, useRef} from "react";
import { fetchAuthor,fetchAuthorById,updateAuthorById,deleteAuthor, postAuthor } from "../services/author_service";
import { uploadImage } from '../services/article_service';

export default function Author() {
  
    const [author, setAuthor] = useState([]);
    const [name,setName] =useState('')
    const [email,setEmail] =useState("")
    const [imageURL, setImageURL] = useState('https://designshack.net/wp-content/uploads/placeholder-image.png')
    const [imageFile, setImageFile] = useState(null)
    useEffect(() => {
        const fetch = async () => {
          let author = await fetchAuthor();
          setAuthor(author);
        };
        fetch();
      });
     
     
      const[idUpdate,setIdUpdate]=useState(0);
      const [btnDisable,setBtnDisable] =useState(0)
      const [btnAddSave,setBtnAddSave] =useState('Add')
      
      const [emailError,setEmailError] =useState('   ')
    
      let addSave;
      btnAddSave==='Add'?addSave=true:addSave=false
      console.log(("ID to update",idUpdate));
      
     const onDelete = (id)=>{
        deleteAuthor(id).then((message)=>{
          let newAuthor = author.filter((author)=>author._id !== id)
          setAuthor(newAuthor)
          alert(message)
        }).catch(e=>console.log(e))
      }
    
    const clearName = React.useRef()
    const clearEmail = React.useRef()
    const clearImg ="https://designshack.net/wp-content/uploads/placeholder-image.png"
    
    const onAdd= async(e)=>{
       e.preventDefault()
       let author ={
         name,email
       }
       if(imageFile){
         let url= await uploadImage(imageFile)
         author.image=url;
         setImageURL(clearImg);
       }
       postAuthor(author).then(message=>alert(message))
       clearName.current.value="";
       clearEmail.current.value="";
       setName('')
       setEmail('')
    }


    let validateEmail=(e)=>{
          setEmail(e.target.value)
          console.log("Email :",email);
          let pattern =/\S+@\S+\.\S+/;
          let result = pattern.test(email);
          if(result){
                 setEmailError('')
          }   
        else{
                setEmailError('Input invalid!')     
      }      
  }

  const showText=(id)=>{
    setIdUpdate(id);
    setBtnDisable(1);
    setBtnAddSave('Save')
    fetchAuthorById(id).then(author=>{
      setName(author.name)
      setEmail(author.email)
      setImageURL(author.image)
    })
  }

  const onUpdate = async(id)=>{
    let author = {
       name,email
    }
    if(imageFile){
       let url = await uploadImage(imageFile)
       author.image = url
    }
    updateAuthorById(id,author).then(message=>alert(message))
    setBtnDisable(0);
    setBtnAddSave('Add')
    clearName.current.value="";
    clearEmail.current.value="";
    setImageURL(clearImg);
       setName('')
       setEmail('')
   }
    
    
  
    return (
        <div>
       <Container>
       <Row> 
       <Col className="mt-5">
       <h1>Author</h1>
        <Form >
            <Form.Group controlId="category">
            <Form.Label><b>Author Name</b></Form.Label>
            <Form.Control ref={clearName} type="text" placeholder="Author Name" value={name}
              onChange={(e)=>setName(e.target.value)}
            />
            <Form.Label  className="mt-2"><b>Email</b></Form.Label>
            <Form.Control  ref={clearEmail} name="email" type="email" placeholder="email" value={email}
                onChange={validateEmail}
            />
             <Form.Text className="text-muted"  ><p style={{color:'red'}}> {emailError}</p></Form.Text>
            </Form.Group>
           
            <Button size="sm mb-3"variant="primary"
               disabled={name==='' || emailError !==''}
               onClick={addSave?onAdd:()=>onUpdate(idUpdate)}
            >{btnAddSave}</Button>

        </Form>
        </Col>
        <Col className="mt-2" >
            <img style={{height:300,width:300}} src={imageURL}/>
            <Form>
            <Form.Group>
                <Form.File 
                    id="img" label="Choose Image" 
                    onChange={(e)=>{
                        let url = URL.createObjectURL(e.target.files[0])
                        setImageFile(e.target.files[0])
                        setImageURL(url)
                    }}
                    />
            </Form.Group>
            </Form>
        </Col>
        </Row> 
        <Table striped bordered hover>
        <thead>
          <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Email</th>
            <th>Image</th>
            <th style={{width:140}}>Action</th>
          </tr>
        </thead>
        <tbody>
        {
        author.map((item,index)=>(
        <tr key={index}>
            <th scope="row">{item._id}</th>
            <td>{item.name}</td>
            <td>{item.email}</td>
            <td >{<img style={{height:150,width:150}} src={item.image}/>}</td>
            <td>
              <Button size="sm mr-2 mb-2 mt-2" variant="warning"
                onClick={
                  ()=>{
                   showText(item._id)
                  }
                }
              >Edit</Button>{" "}
              <Button size="sm " variant="danger" 
              disabled={btnDisable===1}
              onClick={()=>onDelete(item._id)} > Delete</Button>
            </td>
        </tr>
        ))}
           
        </tbody>
      </Table>   
      </Container> 
        </div>
    )
}
