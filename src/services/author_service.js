import api from "../api/api"
export const fetchAuthor = async () => {
    let response = await api.get('author')
    return response.data.data
}

export const deleteAuthor = async (id) => {
    let response = await api.delete('author/'+id)
    return response.data.message
}

export const postAuthor = async (author) => {
    let response = await api.post('author', author)
    return response.data.message
}

export const uploadImage = async (imgFile) => {

    let formData = new FormData()
    formData.append('image', imgFile)

    let response = await api.post('image', formData)
    return response.data.url
}

export const updateAuthorById = async (id, newAuthor) => {
    let response = await api.put('author/' + id, newAuthor)
    return response.data.message
}

export const fetchAuthorById = async (id) => {
    let response = await api.get('author/' + id)
    return response.data.data
}